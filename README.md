<img src="/public/img/Nextjs.png">

# 화투로 보는 오늘의 운세

## 프로젝트 간단 설명
Open [http://localhost:3000](http://localhost:3000)

원하는 화투 두장을 뽑아주세요 <br>
뽑힌 화투를 통해 오늘의 운세를 봐드립니다!

### 프로젝트 실행 화면

#### 첫번째 화면
<img src="/public/img/today-lucky-1.png">
메인 화면입니다. <br>
카드 섞기 버튼을 누르시면 카드가 나타납니다. <br>
얼른 버튼을 눌러보세요!


#### 두번째 화면
<img src="/public/img/today-lucky-2.png">
카드를 뽑으셨군요? <br>
당신이 뽑은 카드는 무엇이 나왔나요? <br>
기회는 한번뿐이지만 당신에게는 특별히 여러 번의 기회를 드리겠습니다!

#### 세번째 화면
<img src="/public/img/today-lucky-3.png">
오늘 당신의 운세는 어떤가요? <br>
저희 화투점은 다른 곳과는 다르게 오늘의 운세와 맞는 음식도 추천해드립니다. <br><br>


또 원하시는 사항이 있으시다면 취향대로 만들어 드릴테니 마음껏 얘기해주세요!

### 사용한 기술
- Redux
- Redux Toolkit
- Redux persist
- React Developer Tools
- axios
- 제미나이 API
- tailwind.config.js
- components