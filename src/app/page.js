'use client'

import {useDispatch, useSelector} from "react-redux";
import {mixingCards, pickCard, resetCard} from "@/lib/features/card/cardSlice";
import axios from "axios";
import {useState} from "react";
import CardItem from "@/components/cardItem";

export default function Home() {
    const {mixCards, pickCards} = useSelector(state => state.card)
    const dispatch = useDispatch()
    const [response, setResponse] = useState('')

    const handleOpenCard = (cardName) => {
        if (pickCards.length < 2) {
            // 2장만 뽑을 수 있기 때문에 조건
            dispatch(pickCard(cardName))
        }
    }

    const handleMixCard = () => {
        dispatch(mixingCards())
    }

    const handleResetCard = () => {
        dispatch(resetCard())
        setResponse('')
    }

    const handleConfirm = () => {
        if (pickCards.length === 2) {
            const keyWord = pickCards.flatMap(card => card.keyWord).join(', ')
            // keyWord를 여러 개 받았기 때문에 모아서 주기
            const command = `${keyWord} 이라는 단어를 가지고 간단하게 오늘의 운세를 한문장으로 만들어줘 결과가 나쁘더라도 최대한 좋은 말만 해줘야돼 그리고 받은 단어에 맞는 음식 추천해줘 그리고 귀엽게 말해줘`
            // 전에는 command를 사용자에게 입력받았는데 미리 지정할 수 있음

            const apiUrl = 'https://generativelanguage.googleapis.com/v1/models/gemini-1.5-flash:generateContent?key=$API_KEY'
            // api 주소 넣기

            const data = {"contents":[{"parts":[{"text": command}]}]}
            // body 준비

            axios.post(apiUrl, data)
                .then(res => {
                    setResponse(res.data.candidates[0].content.parts[0].text)
                    // 데이터 가져오기
                })
                .catch(err => {
                    console.log(err)
                    alert('실패ㅋ')
                })
        }
    }

    return (
      <main className="flex min-h-screen flex-col items-center p-24 bg-orange-400" style={{backgroundImage: `url(/img/쁘이햄뿡.png)`, backgroundSize: "cover", backgroundPosition: "-200%"}}>
          <h1 className="text-8xl mb-10 font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-green-300 via-blue-500 to-purple-600 font-h1-font">오늘의 운세</h1>
          {mixCards.length === 0 ? (
              // 섞인 카드가 없다면 카드부터 섞기
                  <button onClick={handleMixCard} className="text-gray-900 font-h1-font bg-gradient-to-r from-pink-300 via-purple-300 to-indigo-400 to-purple-600 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-red-100 dark:focus:ring-red-400 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2">카드 섞기</button>
          ) : (
              // 섞였을 경우 보여주기
              <div className="mthCardList" onClick={handleOpenCard}>
                  {mixCards.map(card => (
                      <CardItem key={card.name} card={card} handleOpenCard={handleOpenCard} />
                  ))}
                  {pickCards.length === 2 && (
                      // 카드 두개 뽑으면 다시 버튼 생성
                      <div className="flex flex-col items-center justify-center">
                          <button onClick={handleConfirm} className="text-gray-900 font-h1-font bg-gradient-to-r from-pink-300 via-purple-300 to-indigo-400 to-purple-600 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-red-100 dark:focus:ring-red-400 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2">운세 보기</button><br/>
                          {response && <p className="mb-6 bg-slate-50 rounded-lg font-h1-font text-3xl italic underline decoration-indigo-500 decoration-wavy underline-offset-4 leading-snug">{response}</p>}
                          <button onClick={handleResetCard}
                                  className="text-gray-900 font-h1-font bg-gradient-to-r from-pink-300 via-purple-300 to-indigo-400 to-purple-600 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-red-100 dark:focus:ring-red-400 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2">다시
                          </button>
                      </div>
                  )}
              </div>
          )
          }
      </main>
    )
        ;
}
