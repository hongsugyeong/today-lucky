import { configureStore } from '@reduxjs/toolkit'
import cardReducer from "@/lib/features/card/cardSlice";

export default configureStore({
    reducer: {
        card: cardReducer
    }
})