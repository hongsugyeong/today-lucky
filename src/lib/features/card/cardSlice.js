import {createSlice} from '@reduxjs/toolkit'

const initialState = {
    cards: [
        {name: '1월', keyWord: '소식', imgName:'/img/jan.png'},
        {name: '2월', keyWord: '애인', imgName:'/img/feb.png'},
        {name: '3월', keyWord: '만남', imgName:'/img/mar.png'},
        {name: '4월', keyWord: '구설수', imgName:'/img/apr.png'},
        {name: '5월', keyWord: '결혼', imgName:'/img/may.png'},
        {name: '6월', keyWord: '기쁨', imgName:'/img/jun.png'},
        {name: '7월', keyWord: '행운', imgName:'/img/jul.png'},
        {name: '8월', keyWord: '밤', imgName:'/img/aug.png'},
        {name: '9월', keyWord: '술', imgName:'/img/sep.png'},
        {name: '10월', keyWord: '걱정', imgName:'/img/oct.png'},
        {name: '11월', keyWord: '돈', imgName:'/img/nov.png'},
        {name: '12월', keyWord: '손님', imgName:'/img/dec.png'}
    ],
    mixCards: [],
    pickCards: []
}

const cardSlice = createSlice({
    name: 'card',
    initialState,
    reducers: {
        // 카드 초기화
        resetCard: (state) => {
            state.mixCards = []
            state.pickCards = []
        },
        // 카드 섞기
        mixingCards: (state) => {
            state.mixCards = [...state.cards].map(card => ({...card, selected: false}))
            // 원본 복사한 뒤 map 돌려서 각 요소에 selected false 추가하기
            // 카드가 섞인 뒤 선택했을 때 보여주기 때문에 여기서 selected 관리
            for (let i = state.mixCards.length - 1; i > 0; i--) {
                // 거꾸로 돌아가는 for문
                // i는 현재 돌고 있는 번호 (*인덱스 생각하면 될듯)
                const j = Math.floor(Math.random() * (i + 1));
                // j는 랜덤으로 뽑힌 숫자
                // 12개를 랜덤으로 돌리기 때문에  => 0 ~ 11번 카드 뽑을 수 있게 됨
                // Math.random은 0 이상 1 미만의 소수를 무작위로 생성함
                // (i + 1)을 곱해줬기 때문에 0부터 i까지(= 0 이상 i 이하) 무작위로 숫자를 추출할 수 있게 되는 거임
                // Math.floor를 통해 소수를 정수로 내림
                [state.mixCards[i], state.mixCards[j]] = [state.mixCards[j], state.mixCards[i]]
                // 디스트럭처링을 통해 서로 바꿈
                // a, b = b, a => a에 b를 넣고 b에 a 넣기
                // ex) 11 5 => 5 11
            }
            // state.mixCards = [...state.cards].map(card => ({...card, selected: false})).sort(() => Math.random() - 0.5)
            // 한줄로 끝내는 방법임 ㅋ ㅋ
        },
        // 카드 뽑기
        pickCard: (state, action) => {
            const cardIndex = state.mixCards.findIndex(card => card.name === action.payload)
            // 뽑은 카드와 이름이 일치하는 것 찾기
            if (cardIndex !== -1) {
                // -1은 못 찾았을 경우를 나타냄 =not> 찾았을 경우
                state.mixCards[cardIndex].selected = true;
                // mixCards에 해당하는 인덱스 찾아가 true로 바꾸기
                // 굳이 굳이 pickCards에 넣지 않고 mixCards에 넣는 이유는,, 바로롱 카드를 섞었을 때 selected 값을 넣어줬기 때문임
                state.pickCards.push(state.mixCards[cardIndex]);
                // pickCards에 데이터 넣기
            }
        }
    }
})

export default cardSlice.reducer
export const {resetCard, mixingCards, pickCard} = cardSlice.actions