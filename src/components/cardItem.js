export default function CardItem({card, handleOpenCard}) {
    return (
        <div key={card.name} onClick={() => handleOpenCard(card.name)}>
            {card.selected ? (
                <div>
                    <img src={card.imgName} className="mthImgResize mr-1 ml-1 mb-4"/>
                    <p className="font-h1-font">{card.name}</p>
                    <p className="font-h1-font">{card.keyWord}</p>
                </div>
            ) : (
                <div>
                    <img src={'/img/back.jpg'} className="mthImgResize mr-1 ml-1 mb-4"/>
                </div>
            )}
        </div>
    )
}